@echo off
:: -----------------------------------------------------
:: FPLive - Forensic-Proof Live ToolKit v1.1
:: -----------------------------------------------------

:: -----------------------------------------------------
:: VARIABLEs
:: -----------------------------------------------------
set _CASE=""
set _EXAMINER=""
set _OSARCH=""
set _CASE_DIR=""
set _TARGET_DIR=""

set _NOWTIME=""
set _IS_MEMORY=""
set _IS_NONVOLATILE=""
set _IS_PACKET=""
set _MD5=""
set _LOG=""

:: -----------------------------------------------------
:: Determine the OS Architecture
:: -----------------------------------------------------
	if "%PROCESSOR_ARCHITECTURE%" == "x86" set _OSARCH=32
	if "%PROCESSOR_ARCHITECTURE%" == "AMD64" set _OSARCH=64

:: -----------------------------------------------------
:: Enter the case name
:: -----------------------------------------------------
:ENTER_CASE
	set /p _CASE=Please enter the case name : || GOTO:ENTER_CASE

:: -----------------------------------------------------
:: Enter the examiner name
:: -----------------------------------------------------
:ENTER_EXAMINER
	set /p _EXAMINER=Please enter the examiner's name : || GOTO:ENTER_EXAMINER

:: -----------------------------------------------------
:: Check whether physical memory is acquire or not...
:: -----------------------------------------------------
:ACQUIRE_MEMORY
	set /p _IS_MEMORY=Do you want to acquire physical memory? (y or n) || GOTO:ACQUIRE_MEMORY
	if /i "%_IS_MEMORY%" == "Y" GOTO:ACQUIRE_NONVOLATILE
	if /i "%_IS_MEMORY%" == "N" GOTO:ACQUIRE_NONVOLATILE
	GOTO:ACQUIRE_MEMORY

:: -----------------------------------------------------
:: Check whether non-volatile data is acquire or not...
:: -----------------------------------------------------
:ACQUIRE_NONVOLATILE
	set /p _IS_NONVOLATILE=Do you want to acquire Non-volatile data? (y or n) || GOTO:ACQUIRE_NONVOLATILE
	if /i "%_IS_NONVOLATILE%" == "Y" GOTO:START
	if /i "%_IS_NONVOLATILE%" == "N" GOTO:START
	GOTO:ACQUIRE_NONVOLATILE

:START
echo.
echo *************************************************  
echo *      Forensic-proof Incident Response Kit     * 
echo ************************************************* 
	
:: -----------------------------------------------------
:: Create CASE directory
:: -----------------------------------------------------
	set _CASE_DIR="%~dp0\%_CASE%"
	if not exist %_CASE_DIR% mkdir %_CASE_DIR%

:: -----------------------------------------------------
:: Create TARGET directory (using current time)
:: -----------------------------------------------------
	set _TIME=%TIME::=%
	set _NOWTIME=%DATE%_%_TIME%
	set _TARGET_DIR=%~dp0\%_CASE%\%COMPUTERNAME%
	if not exist %_TARGET_DIR% mkdir %_TARGET_DIR%

:: -----------------------------------------------------
:: Create LOG file
:: -----------------------------------------------------
	set _LOG=%_TARGET_DIR%\FPLive_win.log
	if not exist %_LOG% (
	echo ************************************************* > %_LOG%
	echo *   Forensic-proof Incident Response Kit v0.1   * >> %_LOG%
	echo ************************************************* >> %_LOG%
	echo CASE : %_CASE% >> %_LOG%
	echo EXAMINER : %_EXAMINER% >> %_LOG%
	echo START TIME : %DATE% %TIME% >> %_LOG%
	echo. >> %_LOG%
)

:: -----------------------------------------------------
:: FIRST OF ALL, Acquire PREFETCH files and RecentFileCache.bcf
:: -----------------------------------------------------
	echo ### FIRST OF ALL, START ACQUIRING PREFETCH AND RECENTFILECACHE
	set _NONVOLATILE_DIR=%_TARGET_DIR%\non_volatile
	mkdir %_NONVOLATILE_DIR%
	echo Created "non_volatile" directory in %_TARGET_DIR%\
	echo Created "non_volatile" directory in %_TARGET_DIR%\ >> %_LOG%
	echo ----------------------------------------- >> %_LOG%
	echo # PREFETCH                                >> %_LOG%
	echo ----------------------------------------- >> %_LOG%
	echo %DATE% %TIME% - Acquiring Prefetch files ...
	echo %DATE% %TIME% - Acquiring Prefetch files ... >> %_LOG%
	%~dp0\forecopy_handy -p %_NONVOLATILE_DIR%
	set _APPCOMPAT_DIR=%_NONVOLATILE_DIR%\appcompat
	mkdir %_APPCOMPAT_DIR%
	echo ----------------------------------------- >> %_LOG%
	echo # RecentFileCache.bcf                     >> %_LOG%
	echo ----------------------------------------- >> %_LOG%
	echo %DATE% %TIME% - Acquiring RecentFileCache.bcf ...
	echo %DATE% %TIME% - Acquiring RecentFileCache.bcf ... >> %_LOG%
	%~dp0\forecopy_handy -f %SystemRoot%\AppCompat\Programs\RecentFileCache.bcf %_APPCOMPAT_DIR%

:: -----------------------------------------------------
:: Acquire VOLATILE data 
:: -----------------------------------------------------
	set _VOLATILE_DIR=%_TARGET_DIR%\volatile
	mkdir %_VOLATILE_DIR%
	echo ### START ACQUIRING VOLATILE
	echo Created "volatile" directory in %_TARGET_DIR%\
	echo Created "volatile" directory in %_TARGET_DIR%\ >> %_LOG%

:: NETWORK INFORMATION
	echo ----------------------------------------- >> %_LOG%
	echo # NETWORK INFORMATION                     >> %_LOG%
	echo ----------------------------------------- >> %_LOG%
	set _NETWORK_DIR=%_VOLATILE_DIR%\network_information
	mkdir %_NETWORK_DIR%
	echo # START ACQUIRING NETWORK INFORMATION
	echo Created "network_information" directory in %_VOLATILE_DIR%\
	echo Created "network_information" directory in %_VOLATILE_DIR%\ >> %_LOG%
	echo %DATE% %TIME% - Acquiring arp cache table ...
	echo %DATE% %TIME% - Acquiring arp cache table ... >> %_LOG%
	arp -a > %_NETWORK_DIR%\arp-a.txt
	echo %DATE% %TIME% - Acquiring network Status ...
	echo %DATE% %TIME% - Acquiring network Status ... >> %_LOG%
	netstat -nao > %_NETWORK_DIR%\netstat-nao.txt
	echo %DATE% %TIME% - Acquiring routing Table ...
	echo %DATE% %TIME% - Acquiring routing Table ... >> %_LOG%
	route PRINT > %_NETWORK_DIR%\route_PRINT.txt
	echo %DATE% %TIME% - Acquiring currently opened TCP/IP and UDP ports ...
	echo %DATE% %TIME% - Acquiring currently opened TCP/IP and UDP ports ... >> %_LOG%
	%~dp0\cports /stext %_NETWORK_DIR%\cports.txt
	echo %DATE% %TIME% - Acquiring url protocols ...
	echo %DATE% %TIME% - Acquiring url protocols ... >> %_LOG%
	%~dp0\urlprotocolview /stext %_NETWORK_DIR%\urlprotocolview.txt
	echo %DATE% %TIME% - Acquiring network connected sessions ...
	echo %DATE% %TIME% - Acquiring network connected sessions ... >> %_LOG%
	net sessions > %_NETWORK_DIR%\net_sessions.txt
	echo %DATE% %TIME% - Acquiring network opened files ...
	echo %DATE% %TIME% - Acquiring network opened files ... >> %_LOG%
	net file > %_NETWORK_DIR%\net_file.txt
	echo %DATE% %TIME% - Acquiring network shared information ...
	echo %DATE% %TIME% - Acquiring network shared information ... >> %_LOG%
	net share > %_NETWORK_DIR%\net_share.txt
	echo %DATE% %TIME% - Acquiring NBT(NetBIOS over TCP/IP)'s cache ...
	echo %DATE% %TIME% - Acquiring NBT(NetBIOS over TCP/IP)'s cache ... >> %_LOG%
	nbtstat -c > %_NETWORK_DIR%\nbtstat-c.txt
	echo %DATE% %TIME% - Acquiring NBT(NetBIOS over TCP/IP)'s sessions ...
	echo %DATE% %TIME% - Acquiring NBT(NetBIOS over TCP/IP)'s sessions ... >> %_LOG%
	nbtstat -s > %_NETWORK_DIR%\nbtstat-s.txt
	echo %DATE% %TIME% - Acquiring All endpoints information ...
	echo %DATE% %TIME% - Acquiring All endpoints information ... >> %_LOG%
	%~dp0\tcpvcon -a -c /accepteula > %_NETWORK_DIR%\tcpvcon-a-c.txt

:: PHYSICAL MEMORY
	if /i "%_IS_MEMORY%" == "N" GOTO:PROCESS
	echo ----------------------------------------- >> %_LOG%
	echo # PHYSICAL MEMORY                         >> %_LOG%
	echo ----------------------------------------- >> %_LOG%
	set _MEMORY_DIR=%_TARGET_DIR%\memory
	mkdir %_MEMORY_DIR%
	echo ### START ACQUIRING PHYSICAL MEMORY
	echo Created "memory" directory in %_TARGET_DIR%\ >> %_LOG%
	echo %DATE% %TIME% - Acquiring physical memory ...
	echo %DATE% %TIME% - Acquiring physical memory ... >> %_LOG%
	:: memorize	
	rem echo ^<?xml version=^"1.0^" encoding=^"utf-8^"?^> > %_MEMORY_DIR%\config.txt
	rem echo ^<script xmlns:xsi=^"http://www.w3.org/2001/XMLSchema-instance^" xmlns:xsd=^"http://www.w3.org/2001/XMLSchema^" chaining=^"implicit^"^> >> %_MEMORY_DIR%\config.txt
	rem echo  ^<commands^> >> %_MEMORY_DIR%\config.txt
	rem echo    ^<command xsi:type=^"ExecuteModuleCommand^"^> >> %_MEMORY_DIR%\config.txt
	rem echo      ^<module name=^"w32memory-acquisition^" version=^"1.3.22.2^" /^> >> %_MEMORY_DIR%\config.txt
	rem echo      ^<config xsi:type=^"ParameterListModuleConfig^"^> >> %_MEMORY_DIR%\config.txt
	rem echo        ^<parameters^> >> %_MEMORY_DIR%\config.txt 
	rem echo        ^</parameters^> >> %_MEMORY_DIR%\config.txt
	rem echo      ^</config^> >> %_MEMORY_DIR%\config.txt
	rem echo    ^</command^> >> %_MEMORY_DIR%\config.txt
	rem echo  ^</commands^> >> %_MEMORY_DIR%\config.txt
	rem echo ^</script^> >> %_MEMORY_DIR%\config.txt
	rem START /WAIT Memoryze.exe -o %_MEMORY_DIR% -script %_MEMORY_DIR%\config.txt -encoding none -allowmultiple
    %~dp0\winpmem-2.1.post4.exe --output %_MEMORY_DIR%\physmem.raw


rem :PROCESS
rem :: PROCESS INFORMATION
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # PROCESS INFORMATION                     >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	set _PROCESS_DIR=%_VOLATILE_DIR%\process_information
rem 	mkdir %_PROCESS_DIR%
rem 	echo # START ACQUIRING PROCESS INFORMATION
rem 	echo Created "process_information" directory in %_VOLATILE_DIR%\
rem 	echo Created "process_information" directory in %_VOLATILE_DIR%\ >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring list of all processes_1 ...
rem 	echo %DATE% %TIME% - Acquiring list of all processes_1 ... >> %_LOG%
rem 	%~dp0\PSTools\pslist /accepteula > %_PROCESS_DIR%\pslist.txt
rem 	echo %DATE% %TIME% - Acquiring list of all processes_2 ...
rem 	echo %DATE% %TIME% - Acquiring list of all processes_2 ... >> %_LOG%
rem 	%~dp0\cprocess /stext %_PROCESS_DIR%\cprocess.txt
rem 	echo %DATE% %TIME% - Acquiring list of all processes_3 ...
rem 	echo %DATE% %TIME% - Acquiring list of all processes_3 ... >> %_LOG%
rem 	%~dp0\procinterrogate -ps > %_PROCESS_DIR%\procinterrogate-ps.txt
rem 	echo %DATE% %TIME% - Acquiring list of all processes_4 ...
rem 	echo %DATE% %TIME% - Acquiring list of all processes_4 ... >> %_LOG%
rem 	%~dp0\procinterrogate -list -md5 -ver -o %_PROCESS_DIR%\procinterrogate-list-md5-ver-o.txt
rem 	echo %DATE% %TIME% - Acquiring list of task details ...
rem 	echo %DATE% %TIME% - Acquiring list of task details ... >> %_LOG%
rem 	tasklist -V > %_PROCESS_DIR%\tasklist-V.txt
rem 	echo %DATE% %TIME% - Acquiring command lines for each process ...
rem 	echo %DATE% %TIME% - Acquiring command lines for each process ... >> %_LOG%
rem 	%~dp0\tlist -c > %_PROCESS_DIR%\tlist-c.txt
rem 	echo %DATE% %TIME% - Acquiring task tree ...
rem 	echo %DATE% %TIME% - Acquiring task tree ... >> %_LOG%
rem 	%~dp0\tlist -t > %_PROCESS_DIR%\tlist-t.txt
rem 	echo %DATE% %TIME% - Acquiring services active in each process ...
rem 	echo %DATE% %TIME% - Acquiring services active in each process ... >> %_LOG%
rem 	%~dp0\tlist -s > %_PROCESS_DIR%\tlist-s.txt
rem 	echo %DATE% %TIME% - Acquiring list of loaded DLLs ...
rem 	echo %DATE% %TIME% - Acquiring list of loaded DLLs ... >> %_LOG%
rem 	%~dp0\listdlls /accepteula > %_PROCESS_DIR%\listdlls.txt
rem 	echo %DATE% %TIME% - Acquiring list of all exported functions for specified DLL files ...
rem 	echo %DATE% %TIME% - Acquiring list of all exported functions for specified DLL files ... >> %_LOG%
rem 	%~dp0\dllexp /stext %_PROCESS_DIR%\dllexp.txt
rem 	echo %DATE% %TIME% - Acquiring list of injected DLLs for any process ...
rem 	echo %DATE% %TIME% - Acquiring list of injected DLLs for any process ... >> %_LOG%
rem 	%~dp0\injecteddll /stext %_PROCESS_DIR%\injecteddll.txt
rem 	echo %DATE% %TIME% - Acquiring list of all loaded device drivers ...
rem 	echo %DATE% %TIME% - Acquiring list of all loaded device drivers ... >> %_LOG%
rem 	%~dp0\driverview /stext %_PROCESS_DIR%\driverview.txt
rem 	echo %DATE% %TIME% - Acquiring opened handles for any process ...
rem 	echo %DATE% %TIME% - Acquiring opened handles for any process ... >> %_LOG%
rem 	%~dp0\handle /accepteula > %_PROCESS_DIR%\handle.txt
rem 	echo %DATE% %TIME% - Acquiring list of all opened files ...
rem 	echo %DATE% %TIME% - Acquiring list of all opened files ... >> %_LOG%
rem 	%~dp0\openedfilesview /stext %_PROCESS_DIR%\openfilesview.txt

rem :: LOGON USER INFORMATION
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # LOGON USER INFORMATION                  >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	set _LOGONUSER_DIR=%_VOLATILE_DIR%\logon_user_information
rem 	mkdir %_LOGONUSER_DIR%
rem 	echo # START ACQUIRING LOGON USER INFORMATION
rem 	echo Created "logon_user_information" directory in %_VOLATILE_DIR%\
rem 	echo Created "logon_user_information" directory in %_VOLATILE_DIR%\ >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring logged on users ...
rem 	echo %DATE% %TIME% - Acquiring logged on users ... >> %_LOG%
rem 	%~dp0\PSTools\psloggedon /accepteula > %_LOGONUSER_DIR%\psloggedon.txt
rem 	echo %DATE% %TIME% - Acquiring logon sessions ...
rem 	echo %DATE% %TIME% - Acquiring logon sessions ... >> %_LOG%
rem 	%~dp0\logonsessions /accepteula > %_LOGONUSER_DIR%\logonsessions.txt
rem 	echo %DATE% %TIME% - Acquiring user logged on in the past ...
rem 	echo %DATE% %TIME% - Acquiring user logged on in the past ... >> %_LOG%
rem 	%~dp0\netusers /local /history > %_LOGONUSER_DIR%\netusers_local_history.txt
rem 	echo %DATE% %TIME% - Acquiring user account details ...
rem 	echo %DATE% %TIME% - Acquiring user account details ... >> %_LOG%
rem 	net user > %_LOGONUSER_DIR%\net_user.txt
rem 	echo %DATE% %TIME% - Acquiring date/time that users logged on/off ...
rem 	echo %DATE% %TIME% - Acquiring date/time that users logged on/off ... >> %_LOG%
rem 	%~dp0\winlogonview /stext %_LOGONUSER_DIR%\winlogonview.txt
	
rem :: SYSTEM INFORMATION
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # SYSTEM INFORMATION                      >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	set _SYSTEM_DIR=%_VOLATILE_DIR%\system_information
rem 	mkdir %_SYSTEM_DIR%
rem 	echo # START ACQUIRING SYSTEM INFORMATION
rem 	echo Created "system_information" directory in %_VOLATILE_DIR%\
rem 	echo Created "system_information" directory in %_VOLATILE_DIR%\ >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring local and remote system information ...
rem 	echo %DATE% %TIME% - Acquiring local and remote system information ... >> %_LOG%
rem 	%~dp0\PSTools\psinfo /accepteula > %_SYSTEM_DIR%\psinfo.txt
rem 	echo %DATE% %TIME% - Acquiring disk volume information ...
rem 	echo %DATE% %TIME% - Acquiring disk volume information ... >> %_LOG%
rem 	%~dp0\PSTools\psinfo -d > %_SYSTEM_DIR%\psinfo_d.txt
rem 	echo %DATE% %TIME% - Acquiring list of installed software ...
rem 	echo %DATE% %TIME% - Acquiring list of installed software ... >> %_LOG%
rem 	%~dp0\PSTools\psinfo -s > %_SYSTEM_DIR%\psinfo_s.txt
rem 	echo %DATE% %TIME% - Acquiring list of installed hotfixes ...
rem 	echo %DATE% %TIME% - Acquiring list of installed hotfixes ... >> %_LOG%
rem 	%~dp0\PSTools\psinfo -h > %_SYSTEM_DIR%\psinfo_h.txt
rem 	echo %DATE% %TIME% - Acquiring list of windows updates ...
rem 	echo %DATE% %TIME% - Acquiring list of windows updates ... >> %_LOG%
rem 	%~dp0\wul /stext %_SYSTEM_DIR%\wul.txt
rem 	echo %DATE% %TIME% - Acquiring applied group policies ...
rem 	echo %DATE% %TIME% - Acquiring applied group policies ... >> %_LOG%
rem 	rem gplist > %_SYSTEM_DIR%\gplist.txt
rem 	rem echo %DATE% %TIME% - Acquiring applied RSoP group policies ...
rem 	rem echo %DATE% %TIME% - Acquiring applied RSoP group policies ... >> %_LOG%
rem 	gpresult /Z > %_SYSTEM_DIR%\gpresult_Z.txt
rem 	echo %DATE% %TIME% - Acquiring configured services ...
rem 	echo %DATE% %TIME% - Acquiring configured services ... >> %_LOG%
rem 	%~dp0\psservice /accepteula > %_SYSTEM_DIR%\psservice.txt
rem 	echo %DATE% %TIME% - Acquiring time ranges that your computer was turned on ...
rem 	echo %DATE% %TIME% - Acquiring time ranges that your computer was turned on ... >> %_LOG%
rem 	%~dp0\turnedontimesview /stext %_SYSTEM_DIR%\turnedontimesview.txt
rem 	echo %DATE% %TIME% - Acquiring last activity on this system ...
rem 	echo %DATE% %TIME% - Acquiring last activity on this system ... >> %_LOG%
rem 	%~dp0\lastactivityview /stext %_SYSTEM_DIR%\lastactivityview.txt
rem 	echo %DATE% %TIME% - Acquiring search information in cache and history files of web browser ...
rem 	echo %DATE% %TIME% - Acquiring search information in cache and history files of web browser ... >> %_LOG%
rem 	%~dp0\mylastsearch /stext %_SYSTEM_DIR%\mylastsearch.txt
	
rem :: NETWORK INTERFACE INFORMATION
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # NETWORK INTERFACE INFORMATION           >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	set _INTERFACE_DIR=%_VOLATILE_DIR%\interface_information
rem 	mkdir %_INTERFACE_DIR%
rem 	echo # START ACQUIRING INTERFACE INFORMATION
rem 	echo Created "interface_information" directory in %_VOLATILE_DIR%\
rem 	echo Created "interface_information" directory in %_VOLATILE_DIR%\ >> %_LOG%
rem 	rem echo %DATE% %TIME% - Acquiring promiscuous mode information ...
rem 	rem echo %DATE% %TIME% - Acquiring promiscuous mode information ... >> %_LOG%
rem 	rem promiscdetect > %_INTERFACE_DIR%\promiscdetect.txt
rem 	echo %DATE% %TIME% - Acquiring detaild information for each interface ...
rem 	echo %DATE% %TIME% - Acquiring detaild information for each interface ... >> %_LOG%
rem 	ipconfig /all > %_INTERFACE_DIR%\ipconfig_all.txt 
rem 	echo %DATE% %TIME% - Acquiring contents of the DNS resolver cache ...
rem 	echo %DATE% %TIME% - Acquiring contents of the DNS resolver cache ... >> %_LOG%
rem 	ipconfig /displaydns > %_INTERFACE_DIR%\ipconfig_displaydns.txt 
rem 	echo %DATE% %TIME% - Acquiring MAC address for each interface ...
rem 	echo %DATE% %TIME% - Acquiring MAC address for each interface ... >> %_LOG%
rem 	getmac > %_INTERFACE_DIR%\getmac.txt 
rem 	echo %DATE% %TIME% - Acquiring list of all network interfaces ...
rem 	echo %DATE% %TIME% - Acquiring list of all network interfaces ... >> %_LOG%
rem 	%~dp0\networkinterfacesview /stext %_INTERFACE_DIR%\networkinterfacesview.txt

rem :: PASSWORD
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # STORED PASSWORD INFORMATION             >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	set _PASSWORD_DIR=%_VOLATILE_DIR%\password_information
rem 	mkdir %_PASSWORD_DIR%
rem 	echo # START ACQUIRING PASSWORD INFORMATION
rem 	echo Created "password_information" directory in %_VOLATILE_DIR%\
rem 	echo Created "password_information" directory in %_VOLATILE_DIR%\ >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring password for various email clients ...
rem 	echo %DATE% %TIME% - Acquiring password for various email clients ... >> %_LOG%
rem 	%~dp0\mailpv /stext %_PASSWORD_DIR%\mailpv.txt
rem 	echo %DATE% %TIME% - Acquiring passwords stored behind the bullets in the standard password text-box ...
rem 	echo %DATE% %TIME% - Acquiring passwords stored behind the bullets in the standard password text-box ... >> %_LOG%
rem 	%~dp0\bulletspassview /stext %_PASSWORD_DIR%\bulletspassview.txt
rem 	echo %DATE% %TIME% - Acquiring network passwords stored on your system for the current logged-on user ...
rem 	echo %DATE% %TIME% - Acquiring network passwords stored on your system for the current logged-on user ... >> %_LOG%
rem 	%~dp0\netpass /stext %_PASSWORD_DIR%\netpass.txt
rem 	echo %DATE% %TIME% - Acquiring passwords stored by the web browsers (IE, Firefox, Chrome, Safari, Opera) ...
rem 	echo %DATE% %TIME% - Acquiring passwords stored by the web browsers (IE, Firefox, Chrome, Safari, Opera) ... >> %_LOG%
rem 	%~dp0\webbrowserpassview /stext %_PASSWORD_DIR%\webbrowserpassview.txt
rem 	echo %DATE% %TIME% - Acquiring all wireless network security keys/passwords (WEP/WPA) ...
rem 	echo %DATE% %TIME% - Acquiring all wireless network security keys/passwords (WEP/WPA) ... >> %_LOG%
rem 	%~dp0\wirelesskeyview /stext %_PASSWORD_DIR%\wirelesskeyview.txt
rem 	echo %DATE% %TIME% - Acquiring password stored by Microsoft Remote Desktop Connection utility inside the .rdp files ...
rem 	echo %DATE% %TIME% - Acquiring password stored by Microsoft Remote Desktop Connection utility inside the .rdp files ... >> %_LOG%
rem 	%~dp0\rdpv /stext %_PASSWORD_DIR%\rdpv.txt

rem :: MISCs
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # MISCs                                   >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	set _MISC_DIR=%_VOLATILE_DIR%\misc_information
rem 	mkdir %_MISC_DIR%
rem 	echo # START ACQUIRING MISCELLANEOUS INFORMATION
rem 	echo Created "misc_information" directory in %_VOLATILE_DIR%\
rem 	echo Created "misc_information" directory in %_VOLATILE_DIR%\ >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring schedule tasks ...
rem 	echo %DATE% %TIME% - Acquiring schedule tasks ... >> %_LOG%
rem 	at > %_MISC_DIR%\at.txt 
rem 	echo %DATE% %TIME% - Acquiring detailed property list for all tasks ...
rem 	echo %DATE% %TIME% - Acquiring detailed property list for all tasks ... >> %_LOG%
rem 	schtasks /query /fo list /v > %_MISC_DIR%\schtasks_query_fo_list_v.txt 
rem 	echo %DATE% %TIME% - Acquiring clipboard text ...
rem 	echo %DATE% %TIME% - Acquiring clipboard text ... >> %_LOG%
rem 	%~dp0\pclip > %_MISC_DIR%\pclip.txt
rem 	echo %DATE% %TIME% - Acquiring autoruns information ...
rem 	echo %DATE% %TIME% - Acquiring autoruns information ... >> %_LOG%
rem 	%~dp0\autorunsc /accepteula > %_MISC_DIR%\autorunsc.txt

rem :: -----------------------------------------------------
rem :: Acquire NON-VOLATILE data
rem :: -----------------------------------------------------
rem if /i "%_IS_NONVOLATILE%" == "N" GOTO:PACKET
rem 	echo ### START ACQUIRING NON-VOLATILE

rem :: MBR (Master Boot Record)
rem 	set _MBR=%_NONVOLATILE_DIR%\mbr
rem 	mkdir %_MBR%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # MBR                                     >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring MBR ...
rem 	echo %DATE% %TIME% - Acquiring MBR ... >> %_LOG%
rem 	%~dp0\wbin\dd if=\\.\PhysicalDrive0 of=%_MBR%\MBR bs=512 count=1
	
rem :: VBR (Volume Boot Record)
rem 	set _VBR=%_NONVOLATILE_DIR%\vbr
rem 	mkdir %_VBR%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # VBR                                     >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring VBR ...
rem 	echo %DATE% %TIME% - Acquiring VBR ... >> %_LOG%
rem 	%~dp0\forecopy_handy -f %SystemDrive%\$Boot %_VBR%
	
rem :: $MFT
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # $MFT                                    >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring $MFT ...
rem 	echo %DATE% %TIME% - Acquiring $MFT ... >> %_LOG%
rem 	%~dp0\forecopy_handy -m %_NONVOLATILE_DIR%

rem :: $LogFile
rem 	set _FSLOG=%_NONVOLATILE_DIR%\fslog
rem 	mkdir %_FSLOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # $LogFile                                >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring $LogFile ...
rem 	echo %DATE% %TIME% - Acquiring $LogFile ... >> %_LOG%
rem 	%~dp0\forecopy_handy -f %SystemDrive%\$LogFile %_FSLOG%
	
rem :: REGISTRY
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # REGISTRY                                >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring Registry Hives ...
rem 	echo %DATE% %TIME% - Acquiring Registry Hives ... >> %_LOG%
rem 	%~dp0\forecopy_handy -g %_NONVOLATILE_DIR%

rem :: EVENT LOGS
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # EVENT LOGS                              >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring Event Logs ...
rem 	echo %DATE% %TIME% - Acquiring Event Logs ... >> %_LOG%
rem 	%~dp0\forecopy_handy -e %_NONVOLATILE_DIR%

rem :: RECENT LNKs and JUMPLIST
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # RECENT FOLDER                           >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring Recent LNKs and JumpLists ...
rem 	echo %DATE% %TIME% - Acquiring Recent LNKs and JumpLists ... >> %_LOG%
rem 	%~dp0\forecopy_handy -r "%AppData%\microsoft\windows\recent" %_NONVOLATILE_DIR%

rem :: SYSTEM32/drivers/etc files
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # system32/drivers/etc                    >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring system32/drivers/etc ...
rem 	echo %DATE% %TIME% - Acquiring system32/drivers/etc ... >> %_LOG%
rem 	%~dp0\forecopy_handy -t %_NONVOLATILE_DIR%
	
rem :: systemprofile (\Windows\system32\config\systemprofile)
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # system32/config/systemprofile           >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring system32/config/systemprofile ...
rem 	echo %DATE% %TIME% - Acquiring system32/config/systemprofile ... >> %_LOG%
rem 	%~dp0\forecopy_handy -r "%SystemRoot%\system32\config\systemprofile" %_NONVOLATILE_DIR%

rem :: IE Artifacts
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # IE Artifacts                            >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring IE Artifacts ...
rem 	echo %DATE% %TIME% - Acquiring IE Artifacts ... >> %_LOG%
rem 	%~dp0\forecopy_handy -i %_NONVOLATILE_DIR%
	
rem :: Firefox Artifacts
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # Firefox Artifacts                       >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring Firefox Artifacts ...
rem 	echo %DATE% %TIME% - Acquiring Firefox Artifacts ... >> %_LOG%
rem 	%~dp0\forecopy_handy -x %_NONVOLATILE_DIR%	

rem :: Chrome Artifacts
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # Chrome Artifacts                        >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring Chrome Artifacts ...
rem 	echo %DATE% %TIME% - Acquiring Chrome Artifacts ... >> %_LOG%
rem 	%~dp0\forecopy_handy -c %_NONVOLATILE_DIR%
	
rem :: IconCache
rem 	set _ICONCACHE=%_NONVOLATILE_DIR%\iconcache
rem 	mkdir %_ICONCACHE%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # IconCache                               >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring IconCache.db ...
rem 	echo %DATE% %TIME% - Acquiring IconCache.db ... >> %_LOG%
rem 	%~dp0\forecopy_handy -f %LocalAppData%\IconCache.db %_ICONCACHE%	
	
rem :: Thumbcache
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo # Thumbcache                              >> %_LOG%
rem 	echo ----------------------------------------- >> %_LOG%
rem 	echo %DATE% %TIME% - Acquiring Thumbcache_###.db ...
rem 	echo %DATE% %TIME% - Acquiring Thumbcache_###.db ... >> %_LOG%
rem 	%~dp0\forecopy_handy -r "%LocalAppData%\microsoft\windows\explorer" %_NONVOLATILE_DIR%	

rem :: DO YOU HAVE PLENTY OF TIME???
rem :: Calculate MD5
rem 	::echo %DATE% %TIME% - Calculating MD5 values of acquiring files ...
rem 	::echo %DATE% %TIME% - Calculating MD5 values of acquiring files ... >> %_LOG%
rem 	::set _MD5=%_TARGET_DIR%\MD5.log
rem 	::echo The md5 values of acquiring files > %_MD5%
rem 	::echo ****************************************************** >> %_MD5%
rem 	::md5deep -r %_TARGET_DIR% >> %_MD5%


rem :PACKET
rem ::if /i "%_IS_PACKET%" == "N" GOTO:WRAPUP
rem :: -----------------------------------------------------
rem :: Acquire PACKETs
rem :: -----------------------------------------------------
rem 	::echo ### START ACQUIRING PACKET
rem 	::dumpcap -D
	
rem :::SELECT_NIC
rem ::	set /p _NIC=Enter NIC number you want to acquire...(ex. 1,2,3...)? || GOTO:SELECT_NIC

rem :::GO_PACKET
rem 	::set _PACKET_DIR=%_TARGET_DIR%\packet
rem 	::mkdir %_PACKET_DIR%
rem 	::echo Created "packet" directory in %_TARGET_DIR%\
rem 	::echo Created "packet" directory in %_TARGET_DIR%\ >> %_LOG%

rem 	::echo ----------------------------------------- >> %_LOG%
rem 	::echo # PACKET                                  >> %_LOG%
rem 	::echo ----------------------------------------- >> %_LOG%
rem 	::echo %DATE% %TIME% - Acquiring PACKET ...
rem 	::echo %DATE% %TIME% - Acquiring PACKET ... >> %_LOG%
rem 	::dumpcap -D > %_PACKET_DIR%\NIC_list.txt
rem 	::dumpcap -i %_NIC% -a duration:180 -w %_PACKET_DIR%\NIC_%_NIC%.pcap

rem :: -----------------------------------------------------
rem :: Wrap up...
rem :: -----------------------------------------------------
rem :WRAPUP
rem echo END TIME : %DATE% %TIME% >> %_LOG%
rem echo WOW, Sucessfully finished !!
rem PAUSE